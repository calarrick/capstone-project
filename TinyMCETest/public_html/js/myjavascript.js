/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {



    tinymce.init({
      selector: '#mytextarea'
    });
  

//CODE FOR SCROLL-THEN-FIX HEAD & SIDEBAR BEHAVIOR
//===============================



    $(window).scroll(function () {
        var sidebarOffset = $("#menu-sidebar").offset();
        var sidebarWidth = $("#menu-sidebar").css("width");
                

        if ($(document).scrollTop() > 100) {

            $("#menu-sidebar").removeClass("sidebar-flex");
            $("#menu-sidebar").addClass("sidebar-fixed");
            $("#top-anchor").removeClass("top-anchor-unscrolled");
            $("#top-anchor").addClass("top-anchor-scrolled");
            $(".sidebar-fixed").css("left", sidebarOffset.left);
            $(".sidebar-fixed").css("width", sidebarWidth);

        }
        else {
            $("#menu-sidebar").removeClass("sidebar-fixed");
            $("#menu-sidebar").addClass("sidebar-flex");
            $("#menu-sidebar").css("display", "block");
            $("#menu-sidebar").removeAttr("style");
            $("#top-anchor").removeClass("top-anchor-scrolled");
            $("#top-anchor").addClass("top-anchor-unscrolled");
        }

    });


    //CODE FOR COLLAPSIBLE POSTS
    //==========================
    var previewChars = 200;
    var trailingText = '...';
    var readMoreText = "Read More >>";
    var readLessText = "Minimize <<";

    $(".post-body").each(function () {
        var content = $(this).html();

        if (content.length > previewChars) {
            var previewText = content.substr(0, previewChars);
            var hiddenText = content.substr(previewChars);

            var modifiedHTML = previewText +
                    '<span class="trailing-text">' + trailingText + '</span>' +
                    '<span class="rest-of-post">' + hiddenText + '</span><br/>' +
                    '<a href="" class="read-more-link minimized">' + readMoreText + '</a>';


            $(this).html(modifiedHTML);

        }

    });

    $('.read-more-link').click(function () {
        if ($(this).hasClass('minimized')) {
            $(this).removeClass('minimized');
            $(this).html(readLessText);
        } else {
            $(this).addClass('minimized');
            $(this).html(readMoreText);
        }

        $(this).prev().prev().prev().toggle();
        $(this).prev().prev().toggle();
        return false;
    });


    //CODE FOR TOGGLING SIDEBAR
    //=========================

    $(".menu-icon").click(function () {
        $(".sidebar").toggle();
    });

    $(window).on('resize', function (event) {
        var windowWidth = $(window).width();
        if (windowWidth >= 768) {
            $('sidebar').toggle();
            $('.sidebar').attr("display", "inline");
        }
    });
});
