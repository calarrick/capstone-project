package com.thesoftwareguild.mcepostingtest;

import com.thesoftwareguild.mcepostingtest.dao.PostDao;
import com.thesoftwareguild.mcepostingtest.dto.Post;
import com.thesoftwareguild.mcepostingtest.dto.PostTransfer;
import com.thesoftwareguild.mcepostingtest.dto.PostType;
import static com.thesoftwareguild.mcepostingtest.dto.PostType.POST;
import static com.thesoftwareguild.mcepostingtest.dto.PostType.STATIC_PAGE;
import com.thesoftwareguild.mcepostingtest.dto.PubStatus;
import static com.thesoftwareguild.mcepostingtest.dto.PubStatus.DRAFT;
import java.time.LocalDate;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/"})
public class PostEditorController {

    private final PostDao dao;

    @Inject
    public PostEditorController(PostDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String mainPlaceholder() {

        return "index";

    }

    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String adminHome(Model model) {

        return "admin";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    @ResponseBody
    public Post addPost(@RequestBody PostTransfer post) {

        try {
            post.setPostType(PostType.valueOf(post.getPostTypeString()));
        } catch (Exception e) {

            post.setPostType(POST);
            //to do add more elegant handling, but this provides basic
            //server-side assurance that a client can't damage anything
            //by passing in an invalid post-type or pub-status
        }

        try {
            post.setPubStatus(PubStatus.valueOf(post.getPubStatusString()));
        } catch (Exception e) {
            post.setPubStatus(DRAFT);
        }

        post.setPublishDate(LocalDate.
                parse(post.getPublishDateString(), ISO_LOCAL_DATE));

        dao.addPost(post);
        return post;

    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getPosts() {

        return dao.listPosts().stream().filter(post -> post.getPostType().
                equals(POST))
                .collect(Collectors.toList());

    }
    
    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    @ResponseBody public List<Post> getSinglePost(@PathVariable("id") int id) {
        List<Post> r = new ArrayList<>();
        r.add(dao.getPostById(id));
        return r;

    }

    @RequestMapping(value = "/pages", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getPages() {
        return dao.listPosts().stream().filter(post -> post.getPostType().
                equals(STATIC_PAGE))
                .collect(Collectors.toList());
    }
}
