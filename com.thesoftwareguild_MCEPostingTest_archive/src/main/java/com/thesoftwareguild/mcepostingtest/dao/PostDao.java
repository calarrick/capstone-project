/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mcepostingtest.dao;

import com.thesoftwareguild.mcepostingtest.dto.Post;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface PostDao {
    
    public void addPost(Post post);
    
    public List<Post> listPosts();
    
    public Post getPostById(int id);
    
}
