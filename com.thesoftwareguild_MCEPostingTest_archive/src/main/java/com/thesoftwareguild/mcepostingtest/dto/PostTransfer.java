/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mcepostingtest.dto;

import java.time.LocalDate;

/**
 *
 * @author calarrick
 */
public class PostTransfer implements Post {

    private String postBody;
    private int postId; 
    private String postTitle;
    private String author;
    private LocalDate publishDate;
    private LocalDate lastEditDate;
    private String excerpt;
    private PubStatus pubStatus;
    private PostType postType;
    private String tagsAsString;
    private String[] tags;
    private String postTypeString;
    private String publishDateString;
    private String lastEditDateString;
    private String pubStatusString;
    

    @Override
    public String getPostBody() {
        return postBody;
    }

    @Override
    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    @Override
    public void setPostId(int id) {

        postId = id;
    }
    
    @Override
    public int getPostId(){
        return postId;
    }

    @Override
    public void setPostTitle(String postTitle) {

        this.postTitle = postTitle;

    }

    @Override
    public String getPostTitle() {
        return postTitle;
    }

    @Override
    public void setPostType(PostType type) {

        this.postType = type;
    }

    @Override
    public PostType getPostType() {

        return postType;
    }

    @Override
    public String getAuthor() {

        return author;

    }

    @Override
    public void setAuthor(String author) {

        this.author = author;
    }

    @Override
    public LocalDate getPublishDate() {

        return publishDate;
    }

    @Override
    public LocalDate getLastEditDate() {

        return lastEditDate;
    }

    @Override
    public void setPublishDate(LocalDate date) {

        this.publishDate = date;
    }

    @Override
    public void setLastEditDate(LocalDate editDate) {

        this.lastEditDate = editDate;
    }

    @Override
    public void setPubStatus(PubStatus status) {

        this.pubStatus = status;
    }

    @Override
    public PubStatus getPubStatus() {

        return pubStatus;
    }

    @Override
    public void setTagsAsString(String tagsAsString) {
        this.tagsAsString = tagsAsString;
    }

    @Override
    public String getTagsAsString() {
        return tagsAsString;
    }

    @Override
    public String[] getTags() {
        return tags;
    }

    @Override
    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getPostTypeString() {
        return postTypeString;
    }

    public void setPostTypeString(String postTypeString) {
        this.postTypeString = postTypeString;
    }

    public String getPublishDateString() {
        return publishDateString;
    }

    public void setPublishDateString(String publishDateString) {
        this.publishDateString = publishDateString;
    }

    public String getLastEditDateString() {
        return lastEditDateString;
    }

    public void setLastEditDateString(String lastEditDateString) {
        this.lastEditDateString = lastEditDateString;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getPubStatusString() {
        return pubStatusString;
    }

    public void setPubStatusString(String pubStatusString) {
        this.pubStatusString = pubStatusString;
    }

   

}
