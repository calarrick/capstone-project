/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    //CODE FOR COLLAPSIBLE POSTS
    //==========================
    var previewChars = 200;
    var trailingText = '...';
    var readMoreText = "Read More >>";
    var readLessText = "Minimize <<";

    $(".post-body").each(function () {
        var content = $(this).html();

        if (content.length > previewChars) {
            var previewText = content.substr(0, previewChars);
            var hiddenText = content.substr(previewChars);

            var modifiedHTML = previewText +
                    '<span class="trailing-text">' + trailingText + '</span>'+
                    '<span class="rest-of-post">' + hiddenText + '</span><br/>'+
                    '<a href="" class="read-more-link minimized">'+ readMoreText+'</a>';


            $(this).html(modifiedHTML);

        }
            
    });
    
    $('.read-more-link').click(function() {
       if ($(this).hasClass('minimized')) {
           $(this).removeClass('minimized');
           $(this).html(readLessText);
       } else {
           $(this).addClass('minimized');
           $(this).html(readMoreText);
       }
       
       $(this).prev().prev().prev().toggle();
       $(this).prev().prev().toggle();  
       return false;
    });


    //CODE FOR TOGGLING SIDEBAR
    //=========================
    
    $(".menu-icon").click(function() {
       if ($(".sidebar").hasClass("hiding")){
           $(".sidebar").removeClass("hiding");
       } else {
           $('.sidebar').addClass("hiding");
       }
    });
    
    $(window).on('resize', function(event){
       var windowWidth = $(window).width();
       if (windowWidth >=768 && $('.sidebar').hasClass("hiding")){  
           $('.sidebar').removeClass('hiding');
       } else if (windowWidth<768 && !$('.sidebar').hasClass('hiding')){
           $('.sidebar').addClass('hiding');
       }
    });
    
    
    //CODE FOR AFFIXING TOP-LEVEL NAV
    //===============================
    
    $('.scrolling-header').affix({
        offset: {
           top: $('.page-header').position().top+$('.page-header').outerHeight(true)
        }
    });
});
