/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mcepostingtest.dao;

import com.thesoftwareguild.mcepostingtest.dto.Post;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author calarrick
 */
public class DaoForFrontEndTest implements PostDao {

    int postCounter = 0;
    List<Post> postList = new ArrayList<>();

    @Override
    public void addPost(Post post) {

        post.setPostId(postCounter);
        postCounter++;
        
        postList.add(post);

    }

    @Override
    public List<Post> listPosts() {

        return postList;

    }
    
    @Override
    public Post getPostById(int id){
        for (Post p: postList){
            if (p.getPostId()==id)
                return p;
        }
        return null;
    }

}
