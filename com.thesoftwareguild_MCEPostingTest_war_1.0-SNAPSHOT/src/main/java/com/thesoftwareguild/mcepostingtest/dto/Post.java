/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mcepostingtest.dto;

import java.time.LocalDate;

/**
 *
 * @author apprentice
 */
public interface Post {
    
    
  
    public String getPostBody();

    public void setPostBody(String postBody);
    
    public void setPostId(int id);

    public int getPostId();
    
    public void setPostTitle(String postTitle);
    
    public String getPostTitle();
    
    public void setPostType(PostType type);
    
    public PostType getPostType();
    
    public String getAuthor();
    
    public void setAuthor(String author);
    
    public LocalDate getPublishDate();
    
    public LocalDate getLastEditDate();
    
    public void setPublishDate(LocalDate date);
    
    public void setLastEditDate(LocalDate editDate);
    
    public void setPubStatus(PubStatus status);
    
    public PubStatus getPubStatus();
    
    public void setTagsAsString(String tags);
    
    public String getTagsAsString();
    
    public String[] getTags();
    
    public void setTags(String[] tags);
    
    
    
    
    
}
