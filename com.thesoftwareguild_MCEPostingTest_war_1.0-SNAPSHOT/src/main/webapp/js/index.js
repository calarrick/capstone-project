/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//****************ON PAGE LOAD********************
$.ajax({
    url: "posts"
}).success(function (data, status) {
    loadPosts(data, status);
});
    
    
    
//****************FUNCTIONS***********************
    
//POPULATE PAGE WITH BLOG POSTS

var loadPosts = function (postList, status) {
    clearPosts();

    $.each(postList, function (index, post) {
        $('#post-area').append(
                $('<div class=post">').append(
                $('<h1 class="post-title">').append(
                $('<a href="#" class="post-link" data-post-id="'+post.postId+'">').text(post.postTitle))).append(
                $('<h3 class="post-byline">').text(post.author + ', ' + post.pubDate)).append(
                $('<p class="post-body">').text(post.postBody)).append(
                $('<ul id="post-'+post.postId+'-tags" class="tag-area">')));
        
        $.each(post.tags, function(index, tag){
            $("#post-"+post.postId+"-tags").append(
                    $('<li class="tag">').append(
                        $('<a href="#">').text("#"+tag)));
        });
    });
};


var clearPosts = function () {
    $('#post-area').empty();
};

//TITLE LINKS FOR INDIVIDUAL POSTS
$('#post-area').on('click', '.post-link', function (event){
   event.preventDefault();
   
   $.ajax({
       url: "post/"+$(this).data('post-id')
   }).success(function (data, status){
       loadPosts(data, status);
   });
});

