/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

callForPosts();
callForPages();
tinymce.init({
    selector: 'textarea#post-body',
//      theme: 'modern',
//      width: 600,
//      height: 300,
    plugins: [
        'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'save table contextmenu directionality emoticons template paste textcolor'
    ],
    content_css: 'css/content.css',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
});




$('#add-post').click(function (event) {

    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'post',
        data: JSON.stringify({
            postType: $('input[name=post-type]:checked').val(),
            author: $('#author-opts').val(),
            postTitle: $('#post-title').val(),
            publishDateString: $('#date').val(),
            postBody: $('#post-body').val(),
            pubStatus: $('input[name=pub-status]:checked').val(),
            tags: $('#tags').val().split(',')

        }),
        headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
        'dataType': 'json'
    })

            .success(function (data, status) {
                $('#post-type').val('');
                $('#author').val('');
                $('#date').valueOf(new Date());
                $('#post-body').val('');
                $('#draft-status').val('');
                $('#tags').val('');
            });

    setTimeout(refreshAdminEditLists, 250);
    //without refresh as timeout callback it would frequently 
    //'outpace' the server side and refresh without the added posts

});




function refreshAdminEditLists(){
    
    callForPosts();
    callForPages();
    
    
    
}

function callForPosts() {

    clearSidebarPosts();


    $.ajax({
        url: "posts"
    }).success(function (data, status) {

        loadSidebarPosts(data, status);

    });
}

function callForPages() {


    clearSidebarPages();
    $.ajax({
        url: "pages"

    })
            .success(function (data, status) {
                loadSidebarPages(data, status);
            });
}

function clearSidebarPosts() {
    $('#post-edit-list').empty();
}

function clearSidebarPages() {
    $('#page-edit-list').empty();
}


function loadSidebarPosts(listPosts, status) {


    $.each(listPosts, function (index, post) {


        $('#post-edit-list').append(
                $('<li>').html("<a id=" + post.postId + "_edit_post>"
                + post.postTitle + "</a>, " + post.author + "</br>"
                + post.publishDate.month + " " + post.publishDate.dayOfMonth
                + " " + post.publishDate.year + "<br>"));

        $('#' + post.postId + '_edit_post').click(function () {

            loadPostToEdit(post.postId);

        });
    });
}


function loadSidebarPages(listPages, page) {


    $.each(listPages, function (index, page) {
        $('#page-edit-list').append(
                $('<li>').html("<a id=" + page.postId + "_edit_post>"
                + page.postTitle + "</a>, " + page.author + "</br>"));

        $('#' + page.postId + '_edit_post').click(function () {

            loadPostToEdit(page.postId);

        });
    });
}
