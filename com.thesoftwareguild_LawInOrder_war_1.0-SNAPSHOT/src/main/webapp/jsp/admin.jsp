<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Law in Order</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/pagestyle.css" rel="stylesheet">


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <div class="container">
            <div id ="header-div" class="row">
                <header>
                    <div id="title" class="col-xs-12">
                        <h1>Law in Order</h1>
                    </div>
                    <div id="subtitle" class="col-xs-12">
                        <p>Putting the order in law since 2015</p>
                    </div>
                </header>
            </div>
            <nav id="top-level-nav" class="top-anchor-unscrolled narrow-bar col-xs-12">
                <span class="menu-icon glyphicon glyphicon-menu-hamburger"><a href="" class="toggle-sidebar"></a></span>
                <span class ="menu-icon glyphicon glyphicon-triangle-top"><a href="" class=""></a></span>

            </nav>


            <div class="inner-container row">
                <div id="post-wrapper" class="post-wrapper col-sm-9">

                    <div id="new_post_add_edit">
                        <h3>Enter New Post</h3>

                        <form role="form" name="postSubmit" id="postSubmit">

                            <div class="form-group col-sm-3"><h4>Post Type: </h4>

                                <input type="radio" name="post-type" value="POST" checked> Blog Post
                                <br>
                                <input type="radio" name="post-type" value="STATIC_PAGE"> Static Page
                            </div>

                            <div class="form-group col-sm-4 col-md-4">
                                <h4>Publication Date: </h4>
                                <input type="date" id="date" />
                            </div>

                            <div class="form-group col-sm-4 col-md-4"><h4>Author:</h4>

                                <select name="author-opts" id="author-opts">
                                    <option value="make-selection">Select Existing</option>
                                    <option value="Author McAuthor">Author McAuthor</option>
                                    <option value="Testy Testerson">Testy Testerson</option>
                                    <!-- will script to populate pull-down with users in the system -->
                                </select><br>
                                <input type="text" class="form-control" id="author" placeholder="author" />

                            </div>

                            <div class="form-group col-sm-12">
                                <h4>Post Title: </h4>
                                <input type="text" class="form-control" id="post-title" name="post-title" placeholder="title" />
                            </div>

                            <div class="form-group col-sm-12">
                                <h4>Tags: </h4>
                                Separate by commas<br>
                                <textarea id="tags" name="tags" form="postSubmit"></textarea>
                            </div>

                            <div class="form-group col-sm-12">
                                <h4>Enter Main Post Body:</h4>
                                <textarea id="post-body" name="post-body" form="postSubmit"></textarea>
                            </div>

                            <div class="form-group col-sm-3">
                                <input type="radio" name="pub-status" value="DRAFT" checked> Draft
                                <br>
                                <input type="radio" name="pub-status" value="PUBLISH"> Publish
                            </div>

                            <button type="submit" id="add-post" class="btn btn-default col-sm-4">Submit Post</button>
                        </form>
                    </div>



                </div>     

                <div id="menu-sidebar" class="sidebar sidebar-flex col-sm-3 col-xs-12">
                    <h2>Admin Panel</h2>

                    <h4>Posts</h4>
                    <ul id="post-edit-list" class="post-list sidebar-nav"></ul>

                    <h4>Pages</h4>
                    <ul id="page-edit-list" class="post-list sidebar-nav"></ul>


                    <h4>Most Popular Tags</h4>
                    <ul class="sidebar-nav">

                        <li><a href="#">Political (15)</a></li>
                        <li><a href="#">Supreme Court (12)</a></li>
                        <li><a href="#">Education (10)</a></li>
                        <li><a href="#">Privacy (8)</a></li>
                        <li><a href="#">In The News (5)</a></li>
                    </ul>

                    <h4>Quick Access</h4>
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/">Go to Home Page</a></li>
                        <li><a href="#">About the Author</a></li>
                        <li><a href="#">Archives</a></li>

                    </ul>
                </div>


            </div>

        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
        <script src="${pageContext.request.contextPath}/js/layout.js"></script>
        <script src="${pageContext.request.contextPath}/js/submitAndEdit.js"></script>
    </body>
</html>

