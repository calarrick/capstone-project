<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/custom.tld" prefix="custom" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Law in Order</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/pagestyle.css" rel="stylesheet">

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <div id ="header-div" class="row">
                <header>
                    <div id="title" class="col-xs-12">
                        <h1>Law in Order</h1>
                    </div>
                    <div id="subtitle" class="col-xs-12">
                        <p>Putting the order in law since 2015</p>
                    </div>
                </header>
            </div>
            <div id="nav-row" class="row">
                <div id="top-level-nav" class="hidden-sm hidden-md hidden-lg hidden-xl col-xs-12">
                    <p>Button</p>
                </div>
            </div>

            <div id="display-searched-tag"></div>

            <div class="row">
                <section id="post-area" class="col-xs-12 col-sm-10">


                    <div id="post-list" class="post-list">
                        <div class="single-post post">
                            <h1 class="post-title">
                                ${post.postTitle}
                            </h1>
                            <h3 class="post-byline"><span class="byline-author">${post.author}</span>, 
                                <span class="byline-date"><custom:formatDate pattern="MMMM d, uuuu" value="${post.publishDate}"/></span>

                            </h3>
                            <div class="post-body"> ${post.postBody}
                            </div>
                            <ul class="tag-area tag-list" id=${post.postId}-tags>             
                                <c:forEach  items="${post.tags}">
                                    
                                    <li>
                                        <a href="#" class="tag" data-tag-name=${post.tags[i]}>
                                            ${post.tags[i]}</a>
                                    </li>


                                </c:forEach>          
                            </ul>

                        </div>
                    </div>

                </section>    

                <section id="sidebar" class="sidebar-default hidden-xs col-sm-2">
                    <h4>Most Popular Tags</h4>
                    <ul class="sidebar-nav">

                        <li><a href="#">Political (15)</a></li>
                        <li><a href="#">Supreme Court (12)</a></li>
                        <li><a href="#">Education (10)</a></li>
                        <li><a href="#">Privacy (8)</a></li>
                        <li><a href="#">In The News (5)</a></li>
                    </ul>

                    <h4>Quick Access</h4>
                    <ul>
                        <li><a href="${pageContext.request.contextPath}/admin">Admin Panel</a></li> 
                        <li><a href="${pageContext.request.contextPath}/">Go to Home Page</a></li>
                        <li><a href="#">About the Author</a></li>
                        <li><a href="#">Archives</a></li>

                    </ul>
                </section>
            </div>
            <div class="row">
                <footer>
                    <div id="copyright">
                        <p>&copy; 2015 Lorem Ipsum</p>
                    </div>
                </footer>
            </div>
        </div>


        <!-- Scripts; ordering is important-->
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/index.js"></script>
        <script src="js/layout.js"></script>
    </body>
</html>
