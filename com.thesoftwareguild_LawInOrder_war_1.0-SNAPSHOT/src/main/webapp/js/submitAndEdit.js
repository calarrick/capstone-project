/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

callForPosts();
callForPages();
tinymce.init({
    selector: 'textarea#post-body',
//      theme: 'modern',
//      width: 600,
//      height: 300,

    style_formats: [
        {title: 'Headers', items: [
                {title: 'Header 1', format: 'h3'},
                {title: 'Header 2', format: 'h4'},
                {title: 'Header 3', format: 'h5'},
                {title: 'Header 4', format: 'h6'},
                {title: 'Header 5', format: '<br>strong<br>'},
                {title: 'Header 6', format: '<br>em<br>'}
            ]}],
    //style_formats_merge: true,
    plugins: [
        'advlist autolink link image lists charmap print preview hr anchor spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen nonbreaking'
                ,
        'contextmenu directionality emoticons template paste textcolor'
    ],
    menubar: 'insert',
    schema: 'html5-strict',
    content_css: '',
    toolbar: 'insertfile undo redo | styleselect '
            + '| bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | emoticons'
});


$('#author-opts').change(function () {

    var author = $('#author-opts').val();

    $('#author').val(author);



});



$('#add-post').click(function (event) {

    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'publish',
        data: JSON.stringify({
            postType: $('input[name=post-type]:checked').val(),
            author: $('#author').val(),
            postTitle: $('#post-title').val(),
            publishDateString: $('#date').val(),
            postBody: tinyMCE.activeEditor.getContent(),
            pubStatusString: $('input[name=pub-status]:checked').val(),
            tags: $('#tags').val().split(',')

        }),
        headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
        'dataType': 'json'
    })

            .success(function (data, status) {
                $('#post-type').val('');
                $('#post-title').val('');
                $('#author').val('');
                $('#post-title').val('');
                $('#date').valueOf(new Date());
                $('#post-body').val('');
                $('#draft-status').val('');
                $('#tags').val('');
                tinyMCE.activeEditor.setContent('');
            });

    setTimeout(refreshAdminEditLists, 250);
    //without refresh as timeout callback it would frequently 
    //'outpace' the server side and refresh without the added posts

});




function refreshAdminEditLists() {

    callForPosts();
    callForPages();



}

function callForPosts() {

    clearSidebarPosts();


    $.ajax({
        url: "posts"
    }).success(function (data, status) {

        loadSidebarPosts(data, status);

    });
}

function callForPages() {


    clearSidebarPages();
    $.ajax({
        url: "pages"

    })
            .success(function (data, status) {
                loadSidebarPages(data, status);
            });
}

function clearSidebarPosts() {
    $('#post-edit-list').empty();
}

function clearSidebarPages() {
    $('#page-edit-list').empty();
}


function loadSidebarPosts(listPosts, status) {


    $.each(listPosts, function (index, post) {


        $('#post-edit-list').append(
                $('<li>').html("<a id=" + post.postId + "_edit_post>"
                + post.postTitle + "</a>, " + post.author + "</br>"
                + post.publishDate.month + " " + post.publishDate.dayOfMonth
                + " " + post.publishDate.year + "<br>"));

        $('#' + post.postId + '_edit_post').click(function () {

            loadPostToEdit(post.postId);

        });
    });
}


function loadSidebarPages(listPages, page) {


    $.each(listPages, function (index, page) {
        $('#page-edit-list').append(
                $('<li>').html("<a id=" + page.postId + "_edit_post>"
                + page.postTitle + "</a>, " + page.author + "</br>"));

        $('#' + page.postId + '_edit_post').click(function () {

            loadPostToEdit(page.postId);

        });
    });
}
