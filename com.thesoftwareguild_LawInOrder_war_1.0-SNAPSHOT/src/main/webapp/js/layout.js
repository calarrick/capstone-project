/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



//*********LAYOUT***********

//AFFIXING TOP-LEVEL NAV 
$('#top-level-nav').affix({
    offset: {
        top: $('#header-div').position().top + $('#header-div').outerHeight(true)
    }
});


////CODE FOR COLLAPSIBLE POSTS
////==========================
//
//move to index, needs to be in scope with the post generation
//var previewChars = 200;
//var trailingText = '...';
//var readMoreText = "Read More >>";
//var readLessText = "Minimize <<";
//
//$(".post-excerpt").each(function () {
//    var content = $(this).html();
//
//    if (content.length > previewChars) {
//        var previewText = content.substr(0, previewChars);
//        var hiddenText = content.substr(previewChars);
//
//        var modifiedHTML = previewText +
//                '<span class="trailing-text">' + trailingText + '</span>' +
//                '<span class="rest-of-post">' + hiddenText + '</span><br/>' +
//                '<a href="" class="read-more-link minimized">' + readMoreText + '</a>';
//
//
//        $(this).html(modifiedHTML);
//
//    }
//
//});


//SWITCHING BETWEEN DEFAULT AND FIXED SIDEBAR   
$(document).scroll(function () {
    var topOffset = $('#top-level-nav').position().top + $('#nav-row').outerHeight(true);
    if ($(document).scrollTop() > topOffset && $('#sidebar').hasClass('sidebar-default')) {

        var sidebarWidth = $('#sidebar').width;
        $('#sidebar').removeClass('sidebar-default');
        $('#sidebar').addClass('sidebar-fixed');
        $('#sidebar').css("left", $("#post-area").offset().left + $('#post-area').outerWidth(true));
    } else if ($(document).scrollTop() <= topOffset && $('#sidebar').hasClass('sidebar-fixed')) {
        $('#sidebar').removeClass('sidebar-fixed');
        $('#sidebar').addClass('sidebar-default');
        $('#sidebar').css("left", "auto");
    }


});

//DYNAMICALLY POSITIONING FIXED SIDEBAR
$(window).on('resize', function () {
    if ($('#sidebar').hasClass("sidebar-fixed")) {
        var leftOffset = $("#post-area").offset().left + $('#post-area').outerWidth(true);
        $("#sidebar").css('left', leftOffset);
    }

});
