/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thesoftwareguild.lawinorder.control;

import com.thesoftwareguild.lawinorder.dao.LawInOrderDao;
import com.thesoftwareguild.lawinorder.dao.PostPublisher;
import com.thesoftwareguild.lawinorder.dto.Post;
import static com.thesoftwareguild.lawinorder.dto.PostType.STATIC_PAGE;
import com.thesoftwareguild.lawinorder.dto.Publication;
import com.thesoftwareguild.lawinorder.dto.Role;
import com.thesoftwareguild.lawinorder.dto.Tag;
import com.thesoftwareguild.lawinorder.dto.User;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author calarrick
 */

@Controller
@RequestMapping({"/"})
public class BlogDisplayController {
    
    

    private final LawInOrderDao dao;
    private final PostPublisher pub;

    //SET TO FALSE TO DISABLE RESET FROM FRONT END
    private boolean resetOn = true;


    @Inject
    public BlogDisplayController(LawInOrderDao dao, PostPublisher publisher) {
        this.dao = dao;
        this.pub = publisher;
        
             //temporary hard-coded user and role
        //using this on *first* run of project in your build
        //will provide these
//    User user1 = new User();
//    Role role1 = new Role();
//    user1.setUsername("Author McAuthor");
//    user1.setEmail("author@author.com");
//    user1.setPassword("password");
//    role1.setRoleName("ROLE_ADMIN");
//    dao.addUser(user1);
//    dao.addRole(role1);
        
    }
    
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String mainPostDisplay() {

        
        return "index";

    }
    
      @RequestMapping(value = "/posts", method = RequestMethod.GET)
    @ResponseBody
    public List<Publication> getPosts() {

        return dao.getAllPosts().stream().map(p -> pub.publicationBuilder(p))
                .collect(Collectors.toList());
//delivers all post-connected content. 
//may want streamlined methods where only want info
//from main post table
    }
    
    
    
    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String cleanDB() {
        if (resetOn) {
            dao.cleanDB();
        }
        return "index";
    }
    
    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getSinglePost(@PathVariable("id") int id) {
        List<Post> r = new ArrayList<>();
        r.add(dao.getPostById(id));
        return r;

    }

    @RequestMapping(value = "/pages", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getPages() {
        return dao.getAllPosts().stream().filter(post -> post.getPostType().
                equals(STATIC_PAGE))
                .collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/tag/{tag}", method=RequestMethod.GET)
    @ResponseBody
    public List<Publication> getPostsByTag(@PathVariable("tag") String tag) {
        Tag queryBy = dao.getTagByName(tag);
        return dao.getPostsByTag(queryBy.getTagId()).stream().map(p -> pub.publicationBuilder(p))
                .collect(Collectors.toList());
    }
    
    

}
