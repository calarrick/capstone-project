/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.lawinorder.control;

import com.thesoftwareguild.lawinorder.dao.LawInOrderDao;
import com.thesoftwareguild.lawinorder.dao.PostPublisher;
import com.thesoftwareguild.lawinorder.dto.Post;
import com.thesoftwareguild.lawinorder.dto.PostRelational;
import com.thesoftwareguild.lawinorder.dto.Publication;
import com.thesoftwareguild.lawinorder.dto.StaticPage;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author calarrick
 */
@Controller
@RequestMapping({"/"})
public class SinglePostPageController {

    private final LawInOrderDao dao;
    private final PostPublisher pub;

    @Inject
    public SinglePostPageController(LawInOrderDao dao, PostPublisher publisher) {
        this.dao = dao;
        this.pub = publisher;
    }

    //non-ajax alternative not now fully implemented on front-end
//    @RequestMapping(value = "/{postName}", method = RequestMethod.GET)
//    //@ResponseBody
//    public String viewSingle(@PathVariable("postName") String postName) {
//        
//        
//        postName = postName.replaceAll("-", " ");
//        Publication singlePostPage = (pub.publicationBuilder(dao.getPostByTitle(postName)));
//        
//                
//        return "single";
//    }
    
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public String mainPostDisplay() {
//
//        
//        return "index";
//
//    }
    
    @RequestMapping(value = "/{postUrl}", method = RequestMethod.GET)
    //@ResponseBody
    public ModelAndView viewSingle(@PathVariable("postUrl") String postUrl) {

//        String title = postName.replaceAll("-", " ");
//        title = Jsoup.clean(title, Whitelist.
//                basic());
//
//        Post post;
        
        Publication post;

        try {
            post = pub.publicationBuilder(dao.getStaticPageByUrl(postUrl));

        } catch (NullPointerException e) {

            try {
                post = pub.publicationBuilder(dao.getPostByUrl(postUrl));

            } catch (NullPointerException ex) {

                post = custom404(postUrl);

            }

        }
        
        
        return new ModelAndView("single", "post", post);

        

    }

    private Publication custom404(String postUrl) {
        
    Publication custom404 = new Publication();
    
    custom404.setPostBody("404");
    
    return custom404;
    }

}
