package com.thesoftwareguild.lawinorder.control;

import com.thesoftwareguild.lawinorder.dao.LawInOrderDao;
import com.thesoftwareguild.lawinorder.dao.PostPublisher;
import com.thesoftwareguild.lawinorder.dto.Post;
import com.thesoftwareguild.lawinorder.dto.Publication;
import com.thesoftwareguild.lawinorder.dto.PostType;
import static com.thesoftwareguild.lawinorder.dto.PostType.STATIC_PAGE;
import com.thesoftwareguild.lawinorder.dto.PubStatus;
import static com.thesoftwareguild.lawinorder.dto.PubStatus.DRAFT;
import com.thesoftwareguild.lawinorder.dto.Role;
import com.thesoftwareguild.lawinorder.dto.Tag;
import com.thesoftwareguild.lawinorder.dto.User;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/"})
public class PostEditorController {

    private final LawInOrderDao dao;
    private final PostPublisher pub;



    @Inject
    public PostEditorController(LawInOrderDao dao, PostPublisher publisher) {
        this.dao = dao;
        this.pub = publisher;
        
   

    }

    

    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String adminHome(Model model) {

        return "admin";
    }

    @RequestMapping(value = "/publish", method = RequestMethod.POST)
    @ResponseBody
    public Publication addPublication(@RequestBody Publication newContent) {

        
        newContent.setPostBody(Jsoup.parse(newContent.getPostBody()).toString());
        
        try {
            newContent.setPostType(PostType.valueOf(newContent.getPostTypeString()));
        } catch (Exception e) {

            newContent.setPostType(PostType.POST);
            //to do add more elegant handling, but this provides basic
            //server-side assurance that a client can't damage anything
            //by passing in an invalid newContent-type or pub-status
        }

        try {
            newContent.setPubStatus(PubStatus.valueOf(newContent.getPubStatusString()));
        } catch (Exception e) {
            newContent.setPubStatus(DRAFT);
        }

        //postPublisher will take in the 'full' object from a new publication and
        //process to dtos matched up against relational tables, and 
        //have dao add them to the database
        //will also 'rebuild' new publication from those new components (w/ any 
        //regularization that brings)

        

        return pub.processNewPublication(newContent);//returns 'processed' version of publication
 

    }

  

    

    
    
}
