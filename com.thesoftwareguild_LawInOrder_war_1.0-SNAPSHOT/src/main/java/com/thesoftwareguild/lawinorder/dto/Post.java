/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.lawinorder.dto;

import java.time.LocalDate;

/**
 *
 * @author calarrick
 */
public abstract class Post {

    private int postId;
    private int userId;
    private String postBody;
    private LocalDate lastEditDate;
    private boolean isPrivate;
    private String postTitle;
    private LocalDate publishDate;
    private String postExcerpt;
    private PubStatus pubStatus;
    private PostType postType;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    public LocalDate getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(LocalDate lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public String getPostExcerpt() {
        return postExcerpt;
    }

    public void setPostExcerpt(String excerpt) {
        this.postExcerpt = excerpt;
    }

    public PubStatus getPubStatus() {
        return pubStatus;
    }

    public void setPubStatus(
            PubStatus pubStatus) {
        this.pubStatus = pubStatus;
    }

    public PostType getPostType() {
        return postType;
    }

    public void setPostType(
            PostType postType) {
        this.postType = postType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int id) {
        this.userId = id;
    }

}
