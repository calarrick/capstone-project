/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.lawinorder.model;

import java.time.LocalDate;

/**
 *
 * @author apprentice
 */
public class PostModel {
    private int postId;
    private int userid;
    private String postBody;
    private LocalDate lastEditDate;
    private boolean isPrivate;
    private String postTitle;
    private LocalDate publishDate;
    private String excerpt;
    private PubStatus pubStatus;
    private PostType postType;
    
    
    

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }


    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

   
  
    public LocalDate getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(LocalDate lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public boolean isIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public PubStatus getPubStatus() {
        return pubStatus;
    }

    public void setPubStatus(
            PubStatus pubStatus) {
        this.pubStatus = pubStatus;
    }

    public PostType getPostType() {
        return postType;
    }

    public void setPostType(
            PostType postType) {
        this.postType = postType;
    }
}