/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.lawinorder.dao;

import com.thesoftwareguild.lawinorder.model.Comment;
import com.thesoftwareguild.lawinorder.model.PostModel;
import com.thesoftwareguild.lawinorder.model.Role;
import com.thesoftwareguild.lawinorder.model.StaticPage;
import com.thesoftwareguild.lawinorder.model.Tag;
import com.thesoftwareguild.lawinorder.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class LawInOrderDaoDbImpl implements LawInOrderDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //  USERS
    private static final String SQL_INSERT_USER
            = "insert into users (username, email, password) values (?, ?, ?)";

    private static final String SQL_DELETE_USER
            = "delete from users where user_id = ?";

    private static final String SQL_UPDATE_USER
            = "UPDATE users SET username=?, email=?, password=? "
            + " WHERE user_id=?";

    private static final String SQL_SELECT_USER
            = "select * from users where user_id = ?";

    private static final String SQL_SELECT_USERS_BY_POST_ID
            = "select users.user_id, us.username "
            + "from posts inner join users "
            + "on posts.user_id = users.user_id "
            + "where user_id = ?";

    private static final String SQL_SELECT_ALL_USERS
            = "select * from users";

// ROLES
    private static final String SQL_INSERT_ROLE
            = "insert into roles (role_name) values (?)";
    private static final String SQL_DELETE_ROLE
            = "delete from roles where role_id = ?";
    private static final String SQL_UPDATE_ROLE
            = "update roles set role_name = ? where role_id = ? ";

    private static final String SQL_SELECT_ROLE
            = "Select * from roles where role_id = ?";

    private static final String SQL_SELECT_ALL_ROLES
            = "select * from roles";

    // TAGS
    private static final String SQL_INSERT_TAG
            = "insert into tags (tag_name) values (?)";
    private static final String SQL_DELETE_TAG
            = "delete from tags where tag_id = ?";
    private static final String SQL_UPDATE_TAG
            = "update tags set tag_name = ? where tag_id = ? ";

    private static final String SQL_SELECT_TAG
            = "Select * from tags where tag_id = ?";

    private static final String SQL_SELECT_ALL_TAGS
            = "select * from tags";

    // USER METHODS
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addUser(User user) {
        jdbcTemplate.update(SQL_INSERT_USER,
                user.getUsername(),
                user.getEmail(),
                user.getPassword());

        user.setUserId(jdbcTemplate.
                queryForObject("select LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public void deleteUser(int userId) {

        jdbcTemplate.update(SQL_DELETE_USER, userId);

    }

    @Override
    public User updateUser(User user) {

        jdbcTemplate.update(SQL_UPDATE_USER,
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getUserId());

        return user;

    }

    @Override
    public User getUserById(int userId) {

        try {
            return jdbcTemplate.
                    queryForObject(SQL_SELECT_USER, new UserMapper(), userId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

    }

    @Override
    public List<User> getAllUsers() {

        return jdbcTemplate.query(SQL_SELECT_ALL_USERS, new UserMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addRole(Role role) {

        jdbcTemplate.update(SQL_INSERT_ROLE, role.getRoleName());

        role.setRoleId(jdbcTemplate.
                queryForObject("select LAST_INSERT_ID()", Integer.class));

    }

    @Override
    public void deleteRole(int roleId) {
        jdbcTemplate.update(SQL_DELETE_ROLE, roleId);
    }

    @Override
    public Role updateRole(Role role) {

        jdbcTemplate.update(SQL_UPDATE_ROLE,
                role.getRoleName(),
                role.getRoleId());

        return role;

    }

    @Override
    public Role getRoleById(int roleId) {
        try {
            return jdbcTemplate.
                    queryForObject(SQL_SELECT_ROLE, new RoleMapper(), roleId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Role> getAllRoles() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ROLES, new RoleMapper());
    }

    @Override
    public void addComment(Comment comment
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteComment(int commentId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comment updateComment(Comment comment
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comment getCommentById(int commentId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Comment> getCommentsByUserId(int userId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Comment> getAllComments() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addTag(Tag tag) {
        jdbcTemplate.update(SQL_INSERT_TAG,
                tag.getTagName());

        tag.setTagId(jdbcTemplate.
                queryForObject("select LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public void deleteTag(int tagId) {
        jdbcTemplate.update(SQL_DELETE_TAG, tagId);
    }

    @Override
    public Tag updateTag(Tag tag) {
        jdbcTemplate.update(SQL_UPDATE_TAG, tag.getTagName(),
                tag.getTagId());

        return tag;
    }

    @Override
    public Tag getTagById(int tagId) {
        try {
            return jdbcTemplate.
                    queryForObject(SQL_SELECT_TAG, new TagMapper(), tagId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Tag> getAllTags() {
        return jdbcTemplate.query(SQL_SELECT_ALL_TAGS, new TagMapper());
    }

    @Override
    public List<Tag> getTagsFromPost(int postId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addPost(PostModel post
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletePost(int postId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PostModel updatePost(PostModel post
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PostModel getPostById(int postId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PostModel> getPostsByUserId(int userId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PostModel> getAllPosts() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PostModel> getPostsByTag(int tagId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addStaticPage(StaticPage staticPage
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteStaticPage(int postId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StaticPage updateStaticPage(int postId
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StaticPage getStaticPageByPostId(int postId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StaticPage> getStaticPagesByUserId(int userId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    //helper classes

    private static class UserMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {

            User user = new User();
            user.setUserId(rs.getInt("user_id"));
            user.setUsername(rs.getString("username"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));

            return user;
        }
    }

    private static class RoleMapper implements RowMapper<Role> {
        @Override
        public Role mapRow(ResultSet rs, int i) throws SQLException {

            Role role = new Role();
            role.setRoleId(rs.getInt("role_id"));
            role.setRoleName(rs.getString("role_name"));

            return role;
        }
    }
    
    private static class TagMapper implements RowMapper<Tag> {
        @Override
        public Tag mapRow(ResultSet rs, int i) throws SQLException {

            Tag tag = new Tag();
            tag.setTagId(rs.getInt("tag_id"));
            tag.setTagName(rs.getString("tag_name"));

            return tag;
        }
    }

}
